<?php

/**
 * Syte_Core
 */

declare(strict_types=1);

namespace Syte\Core\Block\Adminhtml\Form\Field;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Exception\LocalizedException;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\View\Helper\SecureHtmlRenderer;
use Magento\Framework\Serialize\SerializerInterface;
use Syte\Core\Model\Constants;

class Validation extends Field
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @param Context $context
     * @param SerializerInterface $serializer
     * @param array $data
     * @param SecureHtmlRenderer|null $secureRenderer
     */
    public function __construct(
        Context $context,
        SerializerInterface $serializer,
        array $data = [],
        ?SecureHtmlRenderer $secureRenderer = null
    ) {
        $this->serializer = $serializer;
        parent::__construct($context, $data, $secureRenderer);
    }

    /**
     * @inheritDoc
     */
    protected function _renderScopeLabel(AbstractElement $element): string
    {
        // Return empty label
        return '';
    }

    /**
     * @inheritDoc
     * @throws LocalizedException
     */
    protected function _getElementHtml(AbstractElement $element): string
    {
        // Replace field markup with validation button
        $title = __('Validate Credentials');
        $endpoint = $this->getUrl('syte/configuration/validate');
        $endpointData = htmlentities(
            $this->serializer->serialize(Constants::SYTE_ACCOUNT_VALIDATION_DATA),
            ENT_QUOTES
        );
        // @codingStandardsIgnoreStart
        $html = <<<TEXT
            <button
                    type="button"
                    title="{$title}"
                    class="button syte-validation-button"
                    onclick="syteValidator.call(this, '{$endpoint}', '{$endpointData}')">
                    <span>{$title}</span>
            </button>
TEXT;
        // @codingStandardsIgnoreEnd

        return $html;
    }
}
