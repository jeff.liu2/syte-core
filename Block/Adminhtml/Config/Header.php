<?php

/**
 * Syte_Core
 */

declare(strict_types=1);

namespace Syte\Core\Block\Adminhtml\Config;

use Magento\Config\Block\System\Config\Form\Fieldset;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Syte\Core\Model\Constants;

class Header extends Fieldset
{
    /**
     * @param AbstractElement $element
     *
     * @return string
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function render(AbstractElement $element): string
    {
        return <<<EOT
<a href="{$this->getHeaderUrl()}">
    <img class="syte-settings-header-img" src="{$this->getViewFileUrl('Syte_Core::images/banner.svg')}" 
        alt="{$this->getHeaderAltText()}" title="{$this->getHeaderAltText()}">
</a>
EOT;
    }

    /**
     * Get header image url
     *
     * @return string
     */
    private function getHeaderUrl(): string
    {
        return Constants::SYTE_URL;
    }

    /**
     * Get header image alt
     *
     * @return string
     */
    private function getHeaderAltText(): string
    {
        return (string)__('Visit Syte');
    }
}
