<?php

/**
 * Syte_Core
 */

declare(strict_types=1);

namespace Syte\Core\Model\Services;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Locale\Resolver;
use Syte\Core\Model\Constants;
use Syte\Core\Model\Config;

class Scripts extends AbstractHelper
{
    /**
     * @var Config
     */
    private $configHelper;

    /**
     * @var Resolver
     */
    private $locale;

    /**
     * @param Context $context
     * @param Config $configHelper
     * @param Resolver $locale
     */
    public function __construct(
        Context $context,
        Config $configHelper,
        Resolver $locale
    ) {
        $this->configHelper = $configHelper;
        $this->locale = $locale;
        parent::__construct($context);
    }

    /**
     * Replace anchors inside script content
     *
     * @param string $content
     * @param int $storeId
     *
     * @return void
     */
    public function replaceScriptContentAnchors(string &$content, int $storeId)
    {
        $this->locale->emulate($storeId);
        $data = [
            Constants::SYTE_SCRIPT_ANCHOR_ACCOUNT_ID =>
                (string)$this->configHelper->getAccountId($storeId),
            Constants::SYTE_SCRIPT_ANCHOR_ACCOUNT_SIG =>
                $this->configHelper->getAccountSignature($storeId),
            Constants::SYTE_SCRIPT_ANCHOR_LANG_CODE =>
                $this->locale->getLocale(),
            Constants::SYTE_SCRIPT_ANCHOR_FEED_NAME =>
                $this->configHelper->getAccountProductFeedName($storeId)
        ];
        $this->locale->revert();
        foreach ($data as $anchor => $value) {
            $content = str_replace($anchor, $value, $content);
        }
    }
}
