<?php
declare(strict_types=1);

namespace Syte\Core\Model\Http;

use Exception;
use LogicException;
use Magento\Framework\HTTP\Client\Curl;
use Psr\Log\LoggerInterface;
use Syte\Core\Model\Constants;

class Client
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Curl
     */
    private $httpClient;

    /**
     * Client constructor
     *
     * @param LoggerInterface $logger
     * @param Curl $httpClient
     */
    public function __construct(
        LoggerInterface $logger,
        Curl $httpClient
    ) {
        $this->httpClient = $httpClient;
        $this->logger = $logger;
    }

    /**
     * Send http request
     *
     * @param array $data
     * @param string $method
     *
     * @return int
     */
    public function sendRequest(array $data, string $method): int
    {
        $result = 500;
        $requestUri = Constants::SYTE_ACCOUNT_VALIDATION_URL;
        $log = [
            'data' => $data,
            'request_uri' => $requestUri,
            'method' => $method,
        ];
        $options = [
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_CONNECTTIMEOUT => 15,
            CURLOPT_TIMEOUT => 15,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_VERBOSE => true,
            CURLOPT_HEADER => true,
            CURLINFO_HEADER_OUT => true
        ];

        try {
            $this->httpClient->setOptions($options);
            switch ($method) {
                case \Zend_Http_Client::GET:
                    $this->httpClient->get($requestUri . '?' . http_build_query($data, '', '&'));
                    break;
                case \Zend_Http_Client::POST:
                    $this->httpClient->addHeader("Content-Type", "application/json");
                    $this->httpClient->post($requestUri . '?' . urldecode(http_build_query($data, '', '&')), []);
                    break;
                default:
                    throw new LogicException(
                        sprintf(
                            'Unsupported HTTP method %s',
                            $method
                        )
                    );
            }

            $result = $this->httpClient->getStatus();
            $log['status'] = $result;
            // phpcs:ignore Magento2.Exceptions.ThrowCatch
        } catch (Exception $e) {
            $this->logger->critical($e);
        } finally {
            $this->logger->debug(var_export($log, true));
        }

        return $result;
    }
}
